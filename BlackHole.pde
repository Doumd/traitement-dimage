

public class BlackHole{
   
    float x;
    float y;
    float radius;
    color Color;
   
   BlackHole(float x, float y, float radius, color Color){
     this.x=x;
     this.y=y;
     this.radius=radius;
     this.Color=Color;
   }
  
  public void draw(){
      noStroke();
     fill(Color);
      ellipse(x, y, radius, radius);
      
      
  } 
}
