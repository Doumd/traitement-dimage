Flock flockA;
Flock flockB;

Sun sun;

color c;

int nbPortal=2;

boolean show=true;

boolean explode = false;

float sX, sY, sW, sH;

ArrayList<Star> stars;

ArrayList plist = new ArrayList();
int MAX = 10;

BlackHole blackHole;

SunCollision collision;

ArrayList<Particle[]> part;

  
   

Boid b, b2;



void setup() {
  size(1000,800,P3D);
  
  flockA = new Flock();
  flockB = new Flock();
  
  sun = new Sun();
  
  stars = new ArrayList<Star>();

  part = new ArrayList<Particle []>();
  
  for (int i = 0; i < 10; i++) {
    c=color(255,0,0);
    flockA.addBoid(new Boid(100,100,c));
  }
  for (int i = 0; i < 10; i++) {
    c=color(0,255,0);
    flockB.addBoid(new Boid(width-100,height-100,c));
  }
  
  for (int i = 0; i < 100; i++) {
    stars.add(new Star());
  }
  
  collision = new SunCollision(width/2, height/2, 135);
  
  blackHole = new BlackHole(random(30, width-100),random(30, height-100),50,color(0));
  
   
  
}

  void draw() {
    
    background(0,0,50);
    
    flockA.run();
    flockB.run();
 
             
     for (int i = 0; i < 100; i++) {
      Star star = stars.get(i);
      star.draw();
  }   
 
    if(show){
    blackHole.draw();
    }else{
    } 
    
   
  
   
    collision.draw();
    
   b = flockA.checkCollision(collision, blackHole);
   b2 = flockB.checkCollision(collision, blackHole);
    
    
    
    
   
     
       
      
       
       if( b!=null ){
         
          Particle [] pickles = new Particle [100];
         
         for (int i=0; i<pickles.length; i++) {
         pickles [i] = new Particle (b.position.x, b.position.y);
       
         }
      
           part.add(pickles);
         
       }
       
       if(b2 != null){
         Particle [] pickles = new Particle [100];
         
         for (int i=0; i<pickles.length; i++) {
           
         pickles [i] = new Particle (b2.position.x, b2.position.y);
       
         }
         
         part.add(pickles);
      
       }
       
       
         
       for(Particle[] p : part){
         
         for (int i=0; i<p.length; i++) {
            if(p[i] != null){
               p[i].update();
            }
          
        }
       
       }
          
       sun.draw();
     
    if(flockA.getBoid() == false && flockB.getBoid() == false){
      setup();
    }
  }
    
    

   





void keyPressed() {
    
      if(keyPressed){            
            if(key=='r'){
                setup();
            }     
      }
}
