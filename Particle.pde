class Particle {
 
  float x;
  float y;
  
  float velX; // speed or velocity
  float velY;
  
  
  Particle (float x, float y) {
   //x and y position to be in middle of screen
    this.x = x;
    this.y = y;
    
    velX = random (-10,10);
    velY = random (-10,10);
    
   
  }
  
  void update () {
    
    x+=velX;
    y+=velY;
    
    fill (255, 0, 0);
    ellipse (x,y,random(1, 3),random(1,3));
  }
  
  
}
