class Sun{
    
    float rotY, rotZ;
    
 
    int i=0;
  
  
    float flatrate = 1;
    float scl = 5;
    float hue=0;
    float r;
    float r2;
 
    Sun(){}
 
  void draw(){
   
    
    
    translate(width/2, height/2); 
    lights();
    
    float pidiv = 120;
    float minrad = 59;
    float maxrad = 68;
     
    
    
        rotY= i*PI/(width>>1);
        rotZ= (i+1)*PI/(height>>1);
        
       
        
        rotateX(rotY);
        rotateY(rotZ);
        
        i++;
       
    for(float a=0; a<PI;a+=PI/pidiv){
  
      beginShape(TRIANGLE_STRIP);
      for(float b=0; b<TWO_PI;b+=PI/pidiv){       
        
        hue = map(r,minrad,maxrad,0,360);
       
        fill(255,140,0);
        
        r = map(noise(scl*cos(b)*cos(a)+minrad,scl*cos(b)*sin(a)+minrad,scl*flatrate*sin(b)+minrad),0,1,minrad,maxrad);  
        r2 = map(noise(scl*cos(b)*cos(a+PI/pidiv)+minrad,scl*cos(b)*sin(a+PI/pidiv)+minrad,scl*flatrate*sin(b)+minrad),0,1,minrad,maxrad);
      
        vertex(r*cos(b)*cos(a),-r*cos(b)*sin(a),flatrate*r*sin(b));
        vertex((r2)*cos(b)*cos(a+PI/pidiv),(-r2)*cos(b)*sin(a+PI/pidiv),flatrate*(r2)*sin(b));
          }
     endShape(CLOSE);
    }
    
  }
  
  
}
