// The Flock (a list of Boid objects)

class Flock {
  ArrayList<Boid> boids; // An ArrayList for all the boids
  
 
  Flock() {
    boids = new ArrayList<Boid>(); // Initialize the ArrayList
  }

  void run() {
    for (Boid b : boids) {
      b.run(boids);  // Passing the entire list of boids to each boid individually
      
     
    }
  }
  
  boolean getBoid(){
    int checker = 0;
   for(int i = 0; i<boids.size(); i++){
      
     if (boids.get(i) != null){
        checker ++;
     }
  }
  
  if(checker > 0){
    return true;
  }else{return false;}
 }

Boid checkCollision(SunCollision collision, BlackHole blackHole){

  for(int i = 0; i<boids.size(); i++){
     
     if (boids.get(i).checkCollision(collision, blackHole)==true){
         Boid b = boids.get(i);
         boids.remove(i); //<>//
         return b;
     }
  }
  return null;
}

  void addBoid(Boid b) {
    boids.add(b);
  }

}
